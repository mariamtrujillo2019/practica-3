package ito.poo.app;
import java.time.LocalDate;
import ito.CuentaBancaria;

public class MyApp {

	public static void main(String[] args) {
		
		CuentaBancaria cuenta1;
	    cuenta1=new CuentaBancaria(1234, "Ximena Mariam Trujillo Perez", 389.30f, LocalDate.now());
	    System.out.println(cuenta1);      
	}

}
